import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-project-azamat60.firebaseio.com/'
});

instance.interceptors.request.use(req => {
  console.log('[In request interceptors]', req);
  return req;
});

instance.interceptors.response.use(res => {
  console.log('[In response success interceptor]', res);
  return res;
}, error => {
  console.log('[In response error interceptor]', error);
  throw error;
});

export default instance;
