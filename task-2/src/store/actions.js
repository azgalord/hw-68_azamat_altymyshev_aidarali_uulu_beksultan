import axios from '../axios-tasks';

export const ADD_TASK = 'ADD_TASK';
export const GET_VALUE = 'GET_VALUE';
export const DELETE_TASK = 'DELETE_TASK';
export const MAKE_TASK_DONE = 'MAKE_TASK_DONE';

export const FETCH_TASKS_REQUEST = 'FETCH_TASKS_REQUEST';
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';

export const getValue = (amount) => {
  return dispatch => {
    dispatch({type: GET_VALUE, amount});
  }
};

export const addTask = (amount) => {
  return dispatch => {
    dispatch({type: ADD_TASK, amount});
    dispatch(sendTask());
  }
};

export const deleteTask = (amount) => {
  return dispatch => {
    dispatch({type: DELETE_TASK, amount});
    dispatch(sendTask());
  }
};

export const makeTaskDone = (amount) => {
  return dispatch => {
    dispatch({type: MAKE_TASK_DONE, amount});
    dispatch(sendTask());
  }
};

export const fetchTasksRequest = () => {
  return {type: FETCH_TASKS_REQUEST};
};

export const fetchTasksSuccess = (tasks) => {
  return {type: FETCH_TASKS_SUCCESS, tasks};
};

export const fetchTasks = () => {
  return dispatch => {
    dispatch(fetchTasksRequest());
    axios.get('/tasks.json').then(response => {
      dispatch(fetchTasksSuccess(response.data));
    })
  }
};

export const sendTask = () => {
  return (dispatch, getState) => {
    const state = getState();
    const tasks = state.tasks;
    axios.put('/tasks.json', tasks)
  }
};
