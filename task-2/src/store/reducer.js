import {ADD_TASK, DELETE_TASK, FETCH_TASKS_SUCCESS, GET_VALUE, MAKE_TASK_DONE} from "./actions";

const initialState = {
  value: '',
  tasks: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_VALUE:
      return {...state, value: action.amount.target.value};
    case ADD_TASK:
      action.amount.preventDefault();
      const tasks = [...state.tasks];
      const task = {
        name: state.value,
        done: false
      };
      tasks.push(task);
      return {
        ...state,
        tasks: tasks,
        value: ''
      };
    case DELETE_TASK:
      const allTasks = [...state.tasks];
      allTasks.splice(action.amount, 1);
      return {...state, tasks: allTasks};
    case MAKE_TASK_DONE:
      const tasksDone = [...state.tasks];
      tasksDone[action.amount].done = !tasksDone[action.amount].done;
      return {...state, tasks: tasksDone};
    case FETCH_TASKS_SUCCESS:
      return {
        ...state,
        tasks: action.tasks
      };
    default:
      return state;
  }
};

export default reducer;
