import React, {Component, Fragment} from 'react';
import Input from './components/Input/Input';
import Item from './components/Item/Item';

import {connect} from 'react-redux';
import {addTask, deleteTask, getValue, fetchTasks, makeTaskDone} from "./store/actions";

import './App.css';

class App extends Component {

    deleteElement = (event, id) => {
        event.preventDefault();
        const items = [...this.state.items];
        for (let key in items) {
            if (items[key].id === id) {
                items.splice(key, 1);
            }
        }

        this.setState({items});
    };

    makeTaskDone = (event, id) => {
        let items = [...this.state.items];
        for (let i = 0; i < items.length; i++) {
            if (items[i].id === id) {
                items[i].done = event.target.checked;
            }
        }

        this.setState({items});
    };

    componentDidMount() {
      this.props.fetchTasks();
    }

    render() {
        const items = this.props.tasks.map((item, id) => (
            <Item
                onChange={() => this.props.makeTaskDone(id)}
                nameOfClass={item.done ? 'uk-card uk-card-primary uk-card-body' : 'uk-card uk-card-default uk-card-body'}
                onClick={() => this.props.deleteTask(id)}
                key={id}
                name={item.name}
            />
        ));

        return (
            <Fragment>
                <Input
                    value={this.props.value}
                    onChange={event => this.props.getValue(event)}
                    onClick={event => this.props.addTask(event)}
                />
                <div className="container">
                    <div className="uk-grid uk-child-width-1-3">
                        {items}
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    value: state.value,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getValue: (event) => dispatch(getValue(event)),
    addTask: (event) => dispatch(addTask(event)),
    deleteTask: (id) => dispatch(deleteTask(id)),
    fetchTasks: () => dispatch(fetchTasks()),
    makeTaskDone: (id) => dispatch(makeTaskDone(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
