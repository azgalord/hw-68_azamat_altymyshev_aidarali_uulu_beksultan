import React from 'react';

import './Input.css';

const Input = props => (
    <div className="uk-navbar-container uk-margin">
        <div className="container">
            <form className="uk-grid">
                <div className="uk-width-3-4">
                    <input value={props.value} onChange={props.onChange} className="uk-input uk-form-width-small" type="text" placeholder="Type text here" />
                </div>
                <div className="uk-width-1-4">
                    <button className="uk-button uk-button-default" onClick={props.onClick}>Add</button>
                </div>
            </form>
        </div>
    </div>
);

export default Input;
