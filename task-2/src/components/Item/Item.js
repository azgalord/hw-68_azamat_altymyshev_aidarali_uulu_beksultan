import React from 'react';

import './Item.css';

const Item = props => (
    <div>
        <div className={props.nameOfClass}>
            <div className="uk-flex uk-flex-between uk-flex-middle uk-margin-bottom">
                <h3 className="uk-card-title">
                    {props.name}
                </h3>
                <input onChange={props.onChange} className="uk-checkbox" type="checkbox"/>
            </div>
            <button onClick={props.onClick} className="uk-button uk-button-danger">Delete</button>
        </div>
    </div>
);

export default Item;
